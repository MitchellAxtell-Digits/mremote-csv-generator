#!/usr/bin/env python3

# Import prerequisites
import sys  # Sys allows for standard input when calling the script
import requests  # Requests allows for gathering of API data from a URL
import json  # JSON allows for processing of JSON-formatted data
import csv  # CSV allows for writing out comma-separated files
import random  # Random will be used to generate random numbers
import os  # OS allows us to determine the working path of the script for the config file
import configparser  # ConfigParser allows us to have variables in a static file
import base64  # Base64 will allow decoding the password in the config file
from joblib import Parallel, delayed  # Joblib allows multithreading
import multiprocessing  # Multiprocessing allows shared values between threads

# Gather the input variables
HSONumber = sys.argv[1]
try:
    FilterString = sys.argv[2]
except:
    FilterString = ""

# Defining paramteters here for easy modification
DeviceTypes = "1,3,5,7,10,13,15,16,18,101,104,152,162,168,169,170,171,180,184,185,187,191,192,195,199,209,210,211,225,251,252,260,309,398,399"
MaxReturn = "1000"

# Load the config file
try:
    configuration = configparser.ConfigParser()
    configuration.read(os.path.expanduser('~/.scriptconfig.txt'))
    ClientID = configuration.get("configuration", "ClientID")
    AuthToken = configuration.get("configuration", "AuthToken")
    StandardUsername = configuration.get("configuration", "StandardUsername")
    StandardPassword = configuration.get("configuration", "StandardPassword")

    StandardPassword = base64.b64decode(StandardPassword).decode("utf-8")
except KeyError:
    print("Unable to load the configuration file- please verify it exists!")
    quit()

# Collate the above into parameters
APIParameters = {"access_token": AuthToken,
                 "client_id": ClientID,
                 "maxReturn": MaxReturn,
                 "types": DeviceTypes,
                 "hsoid": HSONumber,
                 "includeVirtual": False,
                 "includeManagementInterfaces": True}

APIUrl = 'https://api.aws.opennetworkexchange.net/api/v2/devices'
# We'll be using just the auth parameters later, so we'll define them now
AuthParameters = {"access_token": AuthToken, "client_id": ClientID}

# Let's generate a couple IDs for the folders
SwitchFolderID = random.randrange(10000000,99999999)
UnmonitoredSwitchFolderID = random.randrange(10000000,99999999)
TelnetFolderID = random.randrange(10000000,99999999)
UnmonitoredTelnetFolderID = random.randrange(10000000,99999999)
HeadEndFolderID = random.randrange(10000000,99999999)

# The next part is necessary to check later if the telnet/unmonitored folders needs to be created
MultiProcessManager = multiprocessing.Manager()
UnmonitoredSwitchTrack = MultiProcessManager.Namespace()
TelnetFolderTrack = MultiProcessManager.Namespace()
UnmonitoredTelnetFolderTrack = MultiProcessManager.Namespace()
UnmonitoredSwitchTrack.value = True
TelnetFolderTrack.value = True
UnmonitoredTelnetFolderTrack.value = True

# Create lists for reference on logs
AddedList = MultiProcessManager.list()
FailedList = MultiProcessManager.list()

# Define the file
with open(HSONumber + '-mremote.csv', mode='w') as mremote_csv:
    mremote_write = csv.writer(mremote_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    # Write the beginning line defining the columns
    mremote_write.writerow(['Name', 'Id', 'Parent', 'NodeType', 'Description', 'Icon', 'Panel', 'Username', 'Password', 'Domain', 'Hostname', 'Protocol', 'PuttySession', 'Port', 'ConnectToConsole', 'UseCredSsp', 'RenderingEngine', 'ICAEncryptionStrength', 'RDPAuthenticationLevel', 'LoadBalanceInfo', 'Colors', 'Resolution', 'AutomaticResize', 'DisplayWallpaper', 'DisplayThemes', 'EnableFontSmoothing', 'EnableDesktopComposition', 'CacheBitmaps', 'RedirectDiskDrives', 'RedirectPorts', 'RedirectPrinters', 'RedirectSmartCards', 'RedirectSound', 'RedirectKeys', 'PreExtApp', 'PostExtApp', 'MacAddress', 'UserField', 'ExtApp', 'VNCCompression', 'VNCEncoding', 'VNCAuthMode', 'VNCProxyType', 'VNCProxyIP', 'VNCProxyPort', 'VNCProxyUsername', 'VNCProxyPassword', 'VNCColors', 'VNCSmartSizeMode', 'VNCViewOnly', 'RDGatewayUsageMethod', 'RDGatewayHostname', 'RDGatewayUseConnectionCredentials', 'RDGatewayUsername', 'RDGatewayPassword', 'RDGatewayDomain', 'InheritCacheBitmaps', 'InheritColors', 'InheritDescription', 'InheritDisplayThemes', 'InheritDisplayWallpaper', 'InheritEnableFontSmoothing', 'InheritEnableDesktopComposition', 'InheritDomain', 'InheritIcon', 'InheritPanel', 'InheritPassword', 'InheritPort', 'InheritProtocol', 'InheritPuttySession', 'InheritRedirectDiskDrives', 'InheritRedirectKeys', 'InheritRedirectPorts', 'InheritRedirectPrinters', 'InheritRedirectSmartCards', 'InheritRedirectSound', 'InheritResolution', 'InheritAutomaticResize', 'InheritUseConsoleSession', 'InheritUseCredSsp', 'InheritRenderingEngine', 'InheritUsername', 'InheritICAEncryptionStrength', 'InheritRDPAuthenticationLevel', 'InheritLoadBalanceInfo', 'InheritPreExtApp', 'InheritPostExtApp', 'InheritMacAddress', 'InheritUserField', 'InheritExtApp', 'InheritVNCCompression', 'InheritVNCEncoding', 'InheritVNCAuthMode', 'InheritVNCProxyType', 'InheritVNCProxyIP', 'InheritVNCProxyPort', 'InheritVNCProxyUsername', 'InheritVNCProxyPassword', 'InheritVNCColors', 'InheritVNCSmartSizeMode', 'InheritVNCViewOnly', 'InheritRDGatewayUsageMethod', 'InheritRDGatewayHostname', 'InheritRDGatewayUseConnectionCredentials', 'InheritRDGatewayUsername', 'InheritRDGatewayPassword', 'InheritRDGatewayDomain', 'InheritRDPAlertIdleTimeout', 'InheritRDPMinutesToIdleTimeout', 'InheritSoundQuality'])

    # Write the folders to file
    mremote_write.writerow(['HeadEnd', HeadEndFolderID, '', 'Container', '', 'mRemoteNG', 'General', '', '', '', '', 'SSH2', 'Default Settings', '22', 'False', 'True', 'IE', 'EncrBasic', 'NoAuth', '', 'Colors16Bit', 'FitToWindow', 'True', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'DoNotPlay', 'False', '', '', '', '', '', 'CompNone', 'EncHextile', 'AuthVNC', 'ProxyNone', '', '0', '', '', 'ColNormal', 'SmartSAspect', 'False', 'Never', '', 'Yes', '', '', '', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True'])
    mremote_write.writerow(['Switches', SwitchFolderID, '', 'Container', '', 'mRemoteNG', 'General', '', '', '', '', 'SSH2', 'Default Settings', '22', 'False', 'True', 'IE', 'EncrBasic', 'NoAuth', '', 'Colors16Bit', 'FitToWindow', 'True', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'DoNotPlay', 'False', '', '', '', '', '', 'CompNone', 'EncHextile', 'AuthVNC', 'ProxyNone', '', '0', '', '', 'ColNormal', 'SmartSAspect', 'False', 'Never', '', 'Yes', '', '', '', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True'])

# Get the base information for the HSO from the BAP API, and parse it as JSON
HSOInfoRaw = requests.get(APIUrl, params = APIParameters)
HSOInfo = HSOInfoRaw.json()

# Count the number of lines and create a tracking variable
NumLines = len(HSOInfo['devices'])
print(f"\n{str(NumLines)} lines to process- one moment please\n")


def main(DIDInfo, UnmonitoredSwitchTrack, TelnetFolderTrack, UnmonitoredTelnetFolderTrack):
    # Split the info out into individual objects
    DeviceID = DIDInfo['id']
    DeviceType = DIDInfo['type']
    PublicIP = DIDInfo['publicIp']
    MonitorType = DIDInfo['statusType']
    
    # Possible that the public IP is not present- let's get the information from the parent if so
    if PublicIP == "":
        ManagementDID = DIDInfo['managementParent']
        # Check if the DID isn't 0
        if ManagementDID == 0:
            # Tunneled devices won't have a parent
            PublicIP = DIDInfo['ipAddr']
            # FailedList.append(str(DeviceID) + " does not have a valid management parent or a public IP!\n")
            # return
        else:
            # Set the parameters and get the information
            ManagementInfo = requests.get('https://api.aws.opennetworkexchange.net/api/v2/devices/{0}'.format(DIDInfo['managementParent']), params=AuthParameters)
            # Set the public IP based on the information gathered
            try:
                PublicIP = ManagementInfo.json()['publicIp']
            except:
                # The device may no longer exist, so error to the user
                FailedList.append(str(DeviceID) + "- the management parent did not provide the relevant information- please check " + str(ManagementDID) + "!\n")
                return

    # Possible that there is no hostname.  If so, use the NASID
    if DIDInfo['name'] == "":
        DeviceHostname = DIDInfo['nasid']
    else:
        DeviceHostname = DIDInfo['name']

    # Let's check if the filter string is present, and if so, if the device hostname matches this skip it
    if FilterString == "":
        pass
    else:
        if FilterString in DeviceHostname:
            FailedList.append(str(DeviceID) + " matches the filter string- skipping!\n")
            return

    # Find any SSH management items present
    sshManagementEntries = [interface for interface in DIDInfo['managementInterfaces'] if interface['protocol'] == 3]

    # Telnet is a fallback
    telnetManagementEntries = [interface for interface in DIDInfo['managementInterfaces'] if interface['protocol'] == 4]

    if len(sshManagementEntries) > 0:
        # Parse out some information- we'll need to do this as a loop as there are multiple items
        for ManageEntry in sshManagementEntries:

            # Parse the info, default to SD standard credentials
            AdminUsername = ManageEntry['adminUsername'] or StandardUsername
            AdminPassword = ManageEntry['adminPassword'] or StandardPassword
            AdminPort = ManageEntry['adminPort'] or "22"
            AdminProto = ManageEntry['protocol']
            TACACSState = ManageEntry['tacacs']
            if TACACSState or ('tacacs' in AdminPassword.lower()):
                DeviceInheritCredentials = "True"
                AdminUsername = StandardUsername
                AdminPassword = StandardPassword
            else:
                DeviceInheritCredentials = "False"
            break

    elif len(telnetManagementEntries) > 0:
        # Prefer not to use Telnet to public IPs- assume SSH
        if DIDInfo['publicIp'] != "":
            AdminProto = 3
        else:
            AdminProto = 4

        # Parse out some information- we'll need to do this as a loop as there are multiple items
        for ManageEntry in telnetManagementEntries:
            # Parse the info, default to SD standard credentials
            AdminUsername = ManageEntry['adminUsername'] or StandardUsername
            AdminPassword = ManageEntry['adminPassword'] or StandardPassword
            AdminPort = ManageEntry['adminPort'] or "22"
            TACACSState = ManageEntry['tacacs']
            if TACACSState or ('tacacs' in AdminPassword.lower()):
                DeviceInheritCredentials = "True"
                AdminUsername = StandardUsername
                AdminPassword = StandardPassword
            else:
                DeviceInheritCredentials = "False"
            break

    elif DIDInfo['publicIp'] != "":
        # Devices on public IPs may be accessible in SSH using credentials from other management types
        for ManageEntry in DIDInfo['managementInterfaces']:
            # Prefer not to use Telnet to public IPs- assume SSH
            AdminUsername = ManageEntry['adminUsername'] or "admin"
            AdminPassword = ManageEntry['adminPassword'] or "51nGleD"
            AdminPort = "22"
            AdminProto = 3
            TACACSState = ManageEntry['tacacs']
            if TACACSState or ('tacacs' in AdminPassword.lower()):
                DeviceInheritCredentials = "True"
                AdminUsername = "admin"
                AdminPassword = "password"
            else:
                DeviceInheritCredentials = "False"
            break

    else:
        FailedList.append(str(DeviceID) + "- no SSH management listed, and this was not listed with a public IP!\n")
        return

    try:
        # Figure out parent folder for later writes
        if DeviceType in (5, 104, 168, 169, 170, 171, 187, 191, 209, 211, 251, 252, 260, 309, 398, 399):
            if AdminProto == 3:
                # If the device isn't monitored, make a folder for if it needed and assign the parent folder as necessary
                if MonitorType == 0:
                    DeviceParentFolder = UnmonitoredSwitchFolderID
                    if UnmonitoredSwitchTrack.value:
                        with open(HSONumber + '-mremote.csv', mode='a') as mremote_csv:
                            mremote_write = csv.writer(mremote_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            mremote_write.writerow(['Switches-unmonitored', UnmonitoredSwitchFolderID, '', 'Container', '', 'mRemoteNG', 'General', '', '', '', '', 'SSH2', 'Default Settings', '22', 'False', 'True', 'IE', 'EncrBasic', 'NoAuth', '', 'Colors16Bit', 'FitToWindow', 'True', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'DoNotPlay', 'False', '', '', '', '', '', 'CompNone', 'EncHextile', 'AuthVNC', 'ProxyNone', '', '0', '', '', 'ColNormal', 'SmartSAspect', 'False', 'Never', '', 'Yes', '', '', '', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True'])
                        UnmonitoredSwitchTrack.value = False
                else:
                    DeviceParentFolder = SwitchFolderID
            else:
                # If the device isn't monitored, make a folder for if it needed and assign the parent folder as necessary
                if MonitorType == 0:
                    DeviceParentFolder = UnmonitoredTelnetFolderID
                    if TelnetFolderTrack.value:
                        with open(HSONumber + '-mremote.csv', mode='a') as mremote_csv:
                            mremote_write = csv.writer(mremote_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            mremote_write.writerow(['Telnet', TelnetFolderID, '', 'Container', '', 'mRemoteNG', 'General', '', '', '', '', 'Telnet', 'Default Settings', '22', 'False', 'True', 'IE', 'EncrBasic', 'NoAuth', '', 'Colors16Bit', 'FitToWindow', 'True', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'DoNotPlay', 'False', '', '', '', '', '', 'CompNone', 'EncHextile', 'AuthVNC', 'ProxyNone', '', '0', '', '', 'ColNormal', 'SmartSAspect', 'False', 'Never', '', 'Yes', '', '', '', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True'])
                        TelnetFolderTrack.value = False
                    if UnmonitoredTelnetFolderTrack.value:
                        with open(HSONumber + '-mremote.csv', mode='a') as mremote_csv:
                            mremote_write = csv.writer(mremote_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            mremote_write.writerow(['Telnet-unmonitored', UnmonitoredTelnetFolderID, '', 'Container', '', 'mRemoteNG', 'General', '', '', '', '', 'Telnet', 'Default Settings', '22', 'False', 'True', 'IE', 'EncrBasic', 'NoAuth', '', 'Colors16Bit', 'FitToWindow', 'True', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'DoNotPlay', 'False', '', '', '', '', '', 'CompNone', 'EncHextile', 'AuthVNC', 'ProxyNone', '', '0', '', '', 'ColNormal', 'SmartSAspect', 'False', 'Never', '', 'Yes', '', '', '', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True'])
                        UnmonitoredTelnetFolderTrack.value = False
                else:
                    DeviceParentFolder = TelnetFolderID
                    if TelnetFolderTrack.value:
                        with open(HSONumber + '-mremote.csv', mode='a') as mremote_csv:
                            mremote_write = csv.writer(mremote_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                            mremote_write.writerow(['Telnet', TelnetFolderID, '', 'Container', '', 'mRemoteNG', 'General', '', '', '', '', 'Telnet', 'Default Settings', '22', 'False', 'True', 'IE', 'EncrBasic', 'NoAuth', '', 'Colors16Bit', 'FitToWindow', 'True', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'False', 'DoNotPlay', 'False', '', '', '', '', '', 'CompNone', 'EncHextile', 'AuthVNC', 'ProxyNone', '', '0', '', '', 'ColNormal', 'SmartSAspect', 'False', 'Never', '', 'Yes', '', '', '', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True'])
                        TelnetFolderTrack.value = False

        else:
            DeviceParentFolder = HeadEndFolderID
        with open(HSONumber + '-mremote.csv', mode='a') as mremote_csv:
            mremote_write = csv.writer(mremote_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            mremote_write.writerow([DeviceHostname, DeviceID, DeviceParentFolder, 'Connection', '', 'mRemoteNG', 'General', AdminUsername, AdminPassword, '', PublicIP, 'SSH2', 'Default Settings', AdminPort, 'False', 'True', 'IE', 'EncrBasic', 'NoAuth', '', 'Colors16Bit', 'FitToWindow', 'True', 'False', 'False', 'False', 'False', 'True', 'False', 'False', 'False', 'False', 'DoNotPlay', 'False', '', '', '', '', '', 'CompNone', 'EncHextile', 'AuthVNC', 'ProxyNone', '', '0', '', '', 'ColNormal', 'SmartSAspect', 'False', 'Never', '', 'Yes', '', '', '', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', DeviceInheritCredentials, 'False', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', DeviceInheritCredentials, 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True', 'True'])

        # Add a success string
        AddedList.append(str(DeviceHostname) + " (" + str(DeviceID) + ") was added\n")

    except NameError:
        FailedList.append(str(DeviceID) + " does not have any management interfaces listed, and was not on a public IP!\n")


# Most of the processing is defined as a function to allow "return" to break the individual DIDs
Parallel(n_jobs=10, verbose=10)(delayed(main)(DIDInfo, UnmonitoredSwitchTrack, TelnetFolderTrack, UnmonitoredTelnetFolderTrack) for DIDInfo in HSOInfo['devices'])

# Print the number of lines added by reading the output file, removing 4 lines for the static folders and headers
with open(HSONumber + '-mremote.csv') as mremote_csv:
    AddedLines = sum(1 for line in mremote_csv) - 4
print(f"\nHSO {str(HSONumber)} is complete- {str(AddedLines)} devices were added (summary below)")

# Sort the lists and print the output
AddedList.sort()
FailedList.sort()
print(''.join(AddedList))
print("These devices failed to add")
print(''.join(FailedList))