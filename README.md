# MRemote CSV Generator

Generate CSV import files for MRemoteNG 1.76+

To check prerequisites, run "./checkprerequisites-mremote.sh" - this will display the packages needed to run

The input is an HSO number and an optional filter string (anything matching the filter will be excluded)- "./mremotegenerate.py XXXX "FilterString""

The output will be in the called location, and will be named "XXXX-mremote.csv"

Please note that it is suggested that mremote be set for Default Inheritance of all except port number, and SSHv2 as the default connection method